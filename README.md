# NodeJS Challenges - Property getters and setters


## Objectives:


- To know about `OOP` concepts in JavaScript.
- To know about `getters` and `setters` in OOJ.
- To know about `Private`, `Public`, `Static` and `Instance` Variables.

### Theory & Documentation

# Getters and Setters

A function that gets a value from a property is called a `getter`, and one that sets the value of a property is called a `setter`.

This feature has been implemented in ES2015, being able to modify the normal operation of setting or obtaining the value of a property, these are known as `accessor properties`.

# Assigning setter / getter for js object


## Constructor function and Object.defineProperty()

In JavaScript, you can use `Object.defineProperty()` method to add getters and setters.

```javascript
function Testing(){
    this._value = "Jallo"

    Object.defineProperty(this, "value", {
        set: function(b){
            this._value = b
        },
        get: function(){
            return this._value
        }
    })    

}


const a = new Testing()

a.value = "er"

console.log(a.value)    // er
```

## Constructor property and Object.defineProperty using prototype

We can define `setter` and `getter` on the prototype.

```javascript
function Testing(){
    this._value = "Jallo"
}

    
Object.defineProperty(Testing.prototype, "value", {
    set: function(b){
        this._value = b
    },
    get: function(){
        return this._value
    }
})

const a = new Testing()

a.value = "er"

console.log(a.value)    // er
```

## ES6 classes

We can define `getters` and `setters` in the class.

```javascript
class Employee {

    constructor(name) {
        this._name = name
    }

    doWork() {
        return `${this._name} is working`
    }

    get name() {
        return this._name.toUpperCase()
    }

    set name(newName){
        if(newName){ 
            this._name = newName
        }
    }
}

const emp = new Employee("Mary");

emp.name                // uses the get method in the background

emp.name = "New name"   // uses the setter in the background
```

## Why use getters and setters?

There are actually many good reasons to consider using accessors rather than directly exposing fields of a class - beyond just the argument of encapsulation and making future changes easier:

Let's see some reasons:

- Encapsulation of behavior associated with getting or setting the property - this 
  allows additional functionality (like validation) to be added more easily later.
- Hiding the internal representation of the property while exposing a property 
  using an alternative representation.
- Getters and setters can allow different access levels - for example the get may 
  be public, but the set could be protected.


## Private, Public, Static and Instance Variables

`Class fields` are variables that hold information. Fields can be attached to 2 entities:

- Fields on the class instance (instance variable).
- Fields on the class itself (static variable).

The fields also have 2 levels of accessibility:

- Public: the field is accessible anywhere.
- Private: the field is accessible only within the body of the class.


```javascript
function MyClass () {                     // constructor function
  let privateVariable = "foo"             // Private variable 

  this.publicVariable = "bar"             // Public variable, an Instance variable.

  this.privilegedMethod = function () {   // Public Method
    alert(privateVariable);
  };
}

// Instance method will be available to all instance but only load once in memory 
MyClass.prototype.publicMethod = function () {    
  alert(this.publicVariable);
};

// Static variable on the class itself
MyClass.staticProperty = "baz"

//...
const myInstance = new MyClass()

```

## Private variables

`Private` instance fields are declared with `#` names (pronounced "hash names"), which are identifiers prefixed with `#`. The `#` is a part of the name itself. 

`Private` fields are accessible on the class constructor from inside the class declaration itself. They are used for declaration of field names as well as for accessing a field's value. 

`Private instance` methods are methods available on class instances whose access is restricted in the same manner as private instance fields. It is a syntax error to refer to `#` names from out of scope. It is also a syntax error to refer to private fields that were not declared before they were called, or to attempt to remove declared fields with delete. Like public fields, `private` fields are added at construction time in a base class, or at the point where super() is invoked in a subclass.

```javascript
class ClassWithPrivateField {
   #privateField;
}

class ClassWithPrivateMethod {
   #privateMethod() {
      return 'hello world';
   }
}

class ClassWithPrivateStaticField {
   static #PRIVATE_STATIC_FIELD;
}

class ClassWithPrivateStaticMethod {
   static #privateStaticMethod() {
      return 'hello world';
   }
}
```

```javascript
class CarModule {
   #speed = 0
   #milesDriven = 0

   accelerate(amount) {
      // It's virtually impossible for this data to be
      // accidentally accessed.
      this.#speed += amount;
      this.#milesDriven += speed;
   }

   getMilesDriven() {
      return this.#milesDriven;
   }
}

const testCarModule = new CarModule();
testCarModule.accelerate(5);
testCarModule.accelerate(4);
console.log(testCarModule.getMilesDriven());
console.log(testCarModule.speed); //=> undefined -- we would need to access the internal keys to access the variable.
```

The `module design pattern` is very useful in JavaScript because it combines public and private components and it allows us to break a program into smaller components, only exposing what another part of the program should be able to access through a process called `encapsulation`. Through this method, we expose only what needs to be used and can hide the rest of the implementation that doesn’t need to be seen. We can take advantage of function scope to implement this.


```javascript
const CarModule = () => {
   let milesDriven = 0;
   let speed = 0;

   const accelerate = (amount) => {
      speed += amount;
      milesDriven += speed;
   }

   const getMilesDriven = () => milesDriven;

   // Using the "return" keyword, you can control what gets
   // exposed and what gets hidden. In this case, we expose
   // only the accelerate() and getMilesDriven() function.
   return {
      accelerate,
      getMilesDriven
   }
};

const testCarModule = CarModule();
testCarModule.accelerate(5);
testCarModule.accelerate(4);
console.log(testCarModule.getMilesDriven());
```

In this second example, you’ll notice the addition of the `this` keyword. There’s a difference between the ES6 arrow function ( => ) and the traditional function(){ . With the function keyword, you can use `this`, which will be bound to the function itself, whereas arrow functions don’t allow any kind of use of the `this` keyword. Both are equally valid ways to create the module. 

> The core idea is to expose parts that should be accessed and leave other parts that should not be interacted with, hence both public and private data.

```javascript
function CarModule() {
   let milesDriven = 0;
   let speed = 0;

   // In this case, we instead use the "this" keyword,
   // which refers to CarModule
   this.accelerate = (amount) => {
      speed += amount;
      milesDriven += speed;
   }

   this.getMilesDriven = () => milesDriven;
}

const testCarModule = new CarModule();
testCarModule.accelerate(5);
testCarModule.accelerate(4);
console.log(testCarModule.getMilesDriven());
```

Let’s take a look at an example class.

```javascript
class CarModule {
   /*
     milesDriven = 0;
     speed = 0;
   */
   constructor() {
      this.milesDriven = 0;
      this.speed = 0;
   }
   accelerate(amount) {
      this.speed += amount;
      this.milesDriven += this.speed;
   }
   getMilesDriven() {
      return this.milesDriven;
   }
}

const testCarModule = new CarModule();
testCarModule.accelerate(5);
testCarModule.accelerate(4);
console.log(testCarModule.getMilesDriven());
```

In cases where privacy is to prevent collaborators from making some catastrophic mistake, prefixing variables with an underscore `(_)`, despite still being `visible` to the outside, can be sufficient to signal to a developer, “Don’t touch this variable.” So, for example, we now have the following:

```javascript
constructor() {
   this._milesDriven = 0;
   this._speed = 0;
}
```

Technically, there is a method for variable privacy in a class that you can use right now, and that’s placing all variables and methods inside the constructor() function. Let’s take a look.

```javascript
class CarModule {
   constructor() {
      let milesDriven = 0;
      let speed = 0;

      this.accelerate = (amount) => {
         speed += amount;
         milesDriven += speed;
      }

      this.getMilesDriven = () => milesDriven;
   }
}

const testCarModule = new CarModule();
testCarModule.accelerate(5);
testCarModule.accelerate(4);
console.log(testCarModule.getMilesDriven());
console.log(testCarModule.speed); // undefined -- We have true variable privacy now.
```

There’s another, more creative way to go about making a `private` variable, and that’s using `WeakMap()`. Although it may sound similar to Map, the two are very different. While maps can take any type of value as a key, a WeakMap only take objects and deletes the values in the WeakMap when the object key is garbage collected. 

In addition, a `WeakMap` cannot be iterated through, meaning that you must have access to the reference to an object key in order to access a value. This makes it rather useful for creating private variables, since the variables are effectively invisible.

```javascript

const secret = new WeakMap();

class Cat {
    constructor(name) {
        secret.set(this, {_name: name});
    }
    
    get name() {
        return secret.get(this)._name
    }
    
    set name(value) {
        secret.get(this)._name = value
    }

}

const toto = new Cat("Tosti")

console.log(toto._name)     //private property

console.log(toto.name)      //Only by getter
```

```javascript
class CarModule {
   constructor() {
      this.data = new WeakMap();
      this.data.set(this, {
         milesDriven: 0,
         speed: 0
      });
      this.getMilesDriven = () => this.data.get(this).milesDriven;
   }

   accelerate(amount) {
      // In this version, we instead create a WeakMap and
      // use the "this" keyword as a key, which is not likely
      // to be used accidentally as a key to the WeakMap.
      const data = this.data.get(this);
      const speed = data.speed + amount;
      const milesDriven = data.milesDriven + data.speed;
      this.data.set({
         speed,
         milesDriven
      });
   }

}

const testCarModule = new CarModule();
testCarModule.accelerate(5);
testCarModule.accelerate(4);
console.log(testCarModule.getMilesDriven());
console.log(testCarModule.data); //=> WeakMap { [items unknown] } -- This data cannot be accessed easily from the outside!
```

If the intent is to prevent name collisions, there is a useful solution using `Symbol`. These are essentially instances that can behave as unique values that will never be equal to anything else, except its own unique instance. Here’s an example of it in action:

```javascript
class CarModule {
    constructor() {
        this.speedKey = Symbol("speedKey");
        this.milesDrivenKey = Symbol("milesDrivenKey");
        this[this.speedKey] = 0;
        this[this.milesDrivenKey] = 0;
    }

    accelerate(amount) {
        // It's virtually impossible for this data to be
        // accidentally accessed. By no means is it private,
        // but it's well out of the way of anyone who would
        // be implementing this module.
        this[this.speedKey] += amount;
        this[this.milesDrivenKey] += this[this.speedKey];
    }

    getMilesDriven() {
        return this[this.milesDrivenKey];
    }
}

const testCarModule = new CarModule();
testCarModule.accelerate(5);
testCarModule.accelerate(4);
console.log("Test", testCarModule.getMilesDriven());
console.log(testCarModule[Symbol('speedKey')]);                // => undefined -- we would need to access the internal keys to access the variable.
console.log(testCarModule[Reflect.ownKeys(testCarModule)[2]]); // 9 -> Using the Reflect class we CAN get ahold of the actual Symbol instances stashed inside.

//Like the underscore solution, this method more or less relies on naming conventions to prevent confusion.
```

## Summary

- There is a widely known agreement that properties beginning with an underscore 
  `_` are internal and should not be manipulated from outside the object.

- As of ES2017, there's still no perfect way to do private properties. Various 
  approaches have pros and cons. `Scoped variables` are truly private; `scoped WeakMaps` are very private and more practical than scoped variables; `scoped Symbols` are reasonably private and reasonably practical; `underscores` are often private enough and very practical.



# NodeJS Challenges (Instructions) - Solving Problems  

## Object-Oriented Javascript

### Objectives

- To use `Constructor Functions`.
- To use `ES6 Classes`.
- To use `Getters` and `Setters`.
- To use `Private`, `Public`, `Static` and `Instance` variables.

### Description

1. Define `Bootcamp` class using constructor function. It is important to use
   `getter` and `setter` for each variable. Also, consider privacy for each variable. 


```javascript
//constructor function


	// static variables
	
	// instance variables

 

 	// instance method

 	// static method



// static variable


// static method


// instance method


```

Check tests. All tests must be asserted.


```javascript
// *~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
// *~*~*~*~*~*~*~* Tests (Don't Touch) *~*~*~*~*~*~*~*~*
// *~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*



console.log("*~*~*~*~**~*~*~*~* Tests 1-6 *~*~*~*~*~*~*~*~*")


let pyBootcamp = new Bootcamp('Python', 'Cuernavaca');


console.log("Test 1...", Bootcamp.instructor === "Jose");
console.log("Test 2...", pyBootcamp.instructor === undefined);

pyBootcamp.name = "Javascript";

console.log("Test 3...", Bootcamp.name === "Bootcamp");
console.log("Test 4...", pyBootcamp.name === "Javascript");
console.log("Test 5...", pyBootcamp.getMembers() === "Jose Returned Members");

console.log("Test 6...", Bootcamp.showCount() === 1);



console.log("*~*~*~*~**~*~*~*~* Tests 7-9 *~*~*~*~*~*~*~*~*")


let rubyBootcamp = new Bootcamp('Ruby', 'Queretaro');

console.log("Test 7...", Bootcamp.showCount() === 2);

Bootcamp.location = "CDMX";

console.log("Test 8...", Bootcamp.getAddress() === "City: CDMX");

pyBootcamp.location = "Acapulco";

console.log("Test 9...", pyBootcamp.start() === "Javascript Bootcamp is started at Acapulco");



console.log("*~*~*~*~**~*~*~*~* Test Errors *~*~*~*~*~*~*~*~*")


/*
pyBootcamp.getAddress();       							
//TypeError: pyBootcamp.getAddress is not a function

Bootcamp.getMembers()
//TypeError: Bootcamp.getMembers is not a function

*/

```

2. Define `Bootcamp` class using ES6 classes. It is important to use
   `getter` and `setter` for each variable. Also, consider privacy for each variable.

```javascript
//ES6 class


```

Check tests. All tests must be asserted.


```javascript
// *~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
// *~*~*~*~*~*~*~* Tests (Don't Touch) *~*~*~*~*~*~*~*~*
// *~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*



console.log("*~*~*~*~**~*~*~*~* Tests 1-9 *~*~*~*~*~*~*~*~*")



let pyBootcamp = new Bootcamp('Python', 'Cuernavaca');

console.log("Test 1...", Bootcamp.instructor === "Jose");   			
console.log("Test 2...", pyBootcamp.instructor === undefined); 	  	    

pyBootcamp.name = "Javascript";

console.log("Test 3...", Bootcamp.name === "Bootcamp");
console.log("Test 4...", pyBootcamp.name === "Javascript");
console.log("Test 5...", Bootcamp.getMembers() === "Jose Returned Members"); 

Bootcamp.location = "CDMX";

console.log("Test 6...", Bootcamp.getAddress() === "City: CDMX");        				// Returned Address

pyBootcamp.location = "Acapulco";

console.log("Test 7...", pyBootcamp.start() === "Javascript Bootcamp is started at Acapulco");


console.log("Test 8...", pyBootcamp.getMembers() === "Jose Returned Members");

console.log("Test 9...", Bootcamp.showCount() === 1);



console.log("*~*~*~*~**~*~*~*~* Tests 10-11 *~*~*~*~*~*~*~*~*")



let rubyBootcamp = new Bootcamp('Ruby', 'Queretaro');

console.log("Test 10...", Bootcamp.showCount() === 2);


let nodeBootcamp = new Bootcamp('NodeJS', 'Monterrey');

console.log("Test 11...", Bootcamp.showCount() === 3);



console.log("*~*~*~*~**~*~*~*~* Test 12 *~*~*~*~*~*~*~*~*")



//+++ YOUR CODE GOES HERE TO TEST-12




console.log("Test 12...", pyBootcamp.getMembers() === "Rafa Returned Members");



console.log("*~*~*~*~**~*~*~*~* Test Error *~*~*~*~*~*~*~*~*")


// Uncomment to test

/*
pyBootcamp.getAddress();       							
// TypeError: pyBootcamp.getAddress is not a function
*/

```

<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />


> References:



1. [Assigning setter and getter for JS Object.](https://stackoverflow.com/questions/30140280/assigning-setter-getter-for-js-object)

2. [Why use getters and setters accessors.](https://stackoverflow.com/questions/1568091/why-use-getters-and-setters-accessors)

3. [Javascript Classes.](https://dmitripavlutin.com/javascript-classes-complete-guide/)

4. [Private Instance variables.](https://pretagteam.com/question/javascript-private-instance-variables)

5. [Private Properties in JS ES6 Classes.](https://stackoverflow.com/questions/22156326/private-properties-in-javascript-es6-classes)

6. [Class Data Hiding.](https://techsparx.com/nodejs/esnext/class-data-hiding.html)

7. [How can I get the value of a symbol property?](https://stackoverflow.com/questions/50453640/how-can-i-get-the-value-of-a-symbol-property)